// utf-8

/* reload */
function checkBreakpoint() {
	var device;
	var width = window.innerWidth;
	if (960 <= width) {
	device = 'pc';
	} else if (768 <= width && width < 960) {
	device = 'pc';
	} else if (480 <= width && width < 768) {
	device = 'sp';
	} else if (width < 480) {
	device = 'sp';
	}
	return device;
}
$(function() {
	var timer = false;
	var device = checkBreakpoint();
	$(window).on('load resize', function(e) {
	if (e.type == 'resize' && device != checkBreakpoint()) {
	  if (timer !== false) {
	    clearTimeout(timer);
	  }
	  timer = setTimeout(function() {
	    location.reload();
	    return false;
	  }, 200);
	}
	});
});

/* pagetop */
function scrollPageTop(target) {
	var $target = $(target);
	$target.click(function() {
	$('body, html').stop(true, true).animate({
		  'scrollTop': 0
		}, 500);
		return false;
	});
}
$(function() {
	scrollPageTop('.pagetop');
});

/* display after scroll */
function displayAfterScroll(target) {
	var $target = $(target);
	var log = 0;
	$target.hide();
	//
	$(window).scroll(function() {
	var isValid = ($(this).scrollTop() > 100) ? 1 : 0;
	if (log != isValid) {
	  if (isValid == 1) {
	    $target.fadeIn(500);
	  } else {
	    $target.fadeOut(500);
	  }
	}
	log = isValid;
	});
}
$(function() {
	displayAfterScroll('.pagetop');
});

/* smooth scroll */
function smoothScroll(target, height) {
	var $target = ($(target).get(0)) ? $(target) : $('html');
	var position = $target.offset().top - height;
	$('body, html').animate({
		'scrollTop': position
	}, 500, 'swing');
}
$(window).load(function() {
	// out page
	var target = location.hash;
	if (target != '') {
		var height = (checkBreakpoint() == 'sp') ? $('.header_inner').height() : 0;
		smoothScroll(target, height);
	}
	// in page
	$('a[href^=#]').click(function() {
		var target = $(this).attr('href');
		var height = (checkBreakpoint() == 'sp') ? $('.header_inner').height() : 0;
		smoothScroll(target, height);
		return false;
	});
});

/* menu */
function hamburgerMenu(button, target) {
	$('#screen, ' + button).click(function() {
		toggleHamburgerMenu(button, target);
	});
	$(target + ' a').click(function() {
		toggleHamburgerMenu(button, target);
	});
}
function toggleHamburgerMenu(button, target) {
	$(button + ' .icon').toggleClass('active');
	$('body').toggleClass('wrap');
	$(target).slideToggle().toggleClass('open');
		$screen = $('#screen');
	if ($(target).hasClass('open')) {
		$screen.fadeIn(500);
	} else {
		$screen.fadeOut(500);
	}
}
$(function() {
	if (checkBreakpoint() == 'sp') {
		hamburgerMenu('header .menu', '#gnav');
	}
});
/* menu back fixed */
$(function(){
	var state = false;
	var scrollpos;
	
	$('.menu,#screen,#gnav li a').on('click', function(){
	    if(state == false) {
	      scrollpos = $(window).scrollTop();
	      $('body').toggleClass('menu_fixed').css({'top': -scrollpos});
	      state = true;
	    } else {
	      $('body').toggleClass('menu_fixed').css({'top': -scrollpos});
	      $('#main').removeClass('menu_fixed').css({'top': 0});
	      window.scrollTo( 0 , scrollpos );
	      state = false;
	    }
	});
});


//------------------------------------------------------------------------------
/* other */
$(function() {

	// link
	$('.hoge').click(function() {
		window.location = $(this).find('a').attr('href');
		return false;
	});

	// accordion
	$('.open').click(function() {
		$(this).toggleClass('close open').next().slideToggle(200);
	});

	// phone number img
	var ua = navigator.userAgent;
	if (ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1) {
	$('.tel-link img').each(function() {
		var alt = $(this).attr('alt');
		$(this).wrap($('<a>').attr('href', 'tel:' + alt.replace(/-/g, '')));
	});
	}

	// phone number moji
	var ua = navigator.userAgent;
	if (ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
	$('.tel-moji').each(function() {
		var str = $(this).text();
		$(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
	});
	}

	// current location display
	if (checkBreakpoint() == 'pc') { // pc only
	var id = $('body').attr('id');
		$('#gnav li.' + id).addClass('now');
		$('#gnav li.now img').css('opacity', 0);
	}

	// mouseover
	if (checkBreakpoint() == 'pc') { // pc only
	$('.opover').hover(function() {
		$(this).fadeTo(100, 0.6);
	}, function() {
		$(this).fadeTo(100, 1);
	});
	}

	// replace image
	if (checkBreakpoint() == 'sp') { // sp only
	$('.spimg img').each(function() {
		$(this).attr('src', $(this).attr('src').replace('_pc', '_sp'));
	});
	}

});
