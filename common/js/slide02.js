// UTF-8

// サムネイル付きスライド
$(function() {
	//フェイドインフェイドアウト
	var slick1 = $('.slider').slick({
		asNavFor:'.thumbnail',
		dots:false,
		infinite: true,
		// centerMode: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		variableWidth:true,
		autoplay: true,
		swipeToSlide: true,
		responsive: [
		{
			breakpoint: 768,
			settings: {
				variableWidth:false,
				slidesToShow: 1,
				arrows: false,
			}
		}
	]
	});
	$('.thumbnail').slick({
		asNavFor:'.slider',
		focusOnSelect: true,
		arrows: false,
		slidesToShow:1,
		autoplay:true,
		swipeToSlide: true,
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}
	]
	});
});
