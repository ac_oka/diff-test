// UTF-8

// ベーシックスライド
$(function() {
	//フェイドインフェイドアウト
	var slick1 = $('.slider').slick({
        dots:true,
		infinite: true,
		swipeToSlide: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay:true,
		fade: true,
		responsive: [
		{
			breakpoint: 768,
			settings: {
				arrows: false,
			}
		}
	]
    });
});
