// UTF-8

$(function() {
	//フェイドインフェイドアウト
	var slick1 = $('.slider').slick({
		dots:true,
		infinite: true,
		swipeToSlide: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		//arrows: false,
		autoplay:true,
		fade: true
	});
	//カルーセル用
	var slick2 = $('.slider02').slick({
		infinite: true,
		dots:true,
		autoplay:true,
		swipeToSlide: true,
		initialSlide:0,
		slidesToShow: 3,
		slidesToScroll: 1
	});
});