// UTF-8

// サムネイル付きギャラリー
$(function() {
	$(document).on('click','.change LI',function(e){
		var ImgSrc = $(e.target).attr("src");
		var ImgAlt = $(e.target).attr("alt");
		var $thisMainImg = $(e.target).closest(".thumb_box").find(".main_img").find("img");
		$thisMainImg.hide();
		$thisMainImg.attr({src:ImgSrc,alt:ImgAlt})
		$thisMainImg.show();
		return false;
	});
});
